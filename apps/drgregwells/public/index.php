<?php
/**
 * ---------------------------------------------------------------
 * PigeonMVC v0.5
 * ---------------------------------------------------------------
 * @author Tyler Savery <Pigeon Team>
 * @see http://www.pigeonmvc.com
 * @license Unreleased
 * @parcel Bootstrap
 * ---------------------------------------------------------------
 */


define('DS', DIRECTORY_SEPARATOR);
define('APP_ROOT', dirname(dirname(__FILE__)).DS);
define('DOCUMENT_ROOT', dirname(dirname(APP_ROOT)).DS);

require_once(DOCUMENT_ROOT.'houston'.DS.'parcels'.DS.'core'.DS.'construct.php');
Core\Loader::init();
Core\Construct::init();
Core\Router::resolve();
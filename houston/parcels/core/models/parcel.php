<?php
namespace Core\Model;

class Parcel {
	
    static $name = 'Base Parcel';
	static $version = '1.0.0';
    static $author = 'No Author';
	static $dependencies;
	static $description;
	static $routes;
	
	static function get_paths() {
		return static::$routes;
	}
    
}
<?php
/**
 * ---------------------------------------------------------------
 * PigeonMVC v0.5
 * ---------------------------------------------------------------
 * @author Pilot Interactive Inc.
 * @see http://www.pigeonmvc.com
 * @license Unreleased
 * ---------------------------------------------------------------
 * 
 * Scaffolding engine for rapid developent. Extends the Base_Controller
 * to take advantage of lower lower controller operations.
 *
 * @parcel Scaffold_Controller
 */
namespace Core\Controllers;

class Scaffold_Controller extends Base_Controller {
    
    /**
     * The URI string passed by the router.
     *
     * @var array
     */
    public $uri;

    /**
     * The view currently being used.
     *
     * @var string
     */
    public $view;

    /**
     * Central variable to access the model for the scaffold.
     *
     * @var class
     */
    public $model;

    /**
     * The action requested by the user. Can be 'list',
     * 'edit', 'create', 'view', 'delete'.
     *
     * @var string
     */
    public $action;

    /**
     * The original name of the model specified.
     *
     * @var string
     */
    public $model_name;

    /**
     * The ordering of fields.
     *
     * @var array
     */
    public $field_order;

    /**
     * Stores disallowed fields from being rendered, organized into
     * a 2D-array by action ie. array('list' => array('field1', 'field2'));
     *
     * @var array
     */
    public $disallow;

    /**
     * The number of rows to be displayed.
     *
     * @var int
     */
    public $list_count = 25;

    /**
     * Reference to the object for pretty field names.
     *
     * @var array
     */
    public $field_names;

    /**
     * Any SQL query modifiers to be passed into the model.
     *
     * @var array
     */
    public $query_modifiers = array();

    /**
     * Toggle which features to allow and disallow;
     *
     * @var array
     */
    public $allow_create = true;
    public $allow_edit   = true;
    public $allow_delete = true;
    public $allow_list   = true;
    public $allow_view   = true;

    /**
     * The class constructor inits the scaffold functions, columns etc.
     *
     * @param string URI passed in by the router.
     * @param array Any data level accessors.
     */
    public function __construct($uri, $data) {
        parent::__construct($uri, $data);

        if ($this->model_name == '') {
            throw new \Pigeon\Core\Exception('You must use define_model([model_name]) in order to use scaffolding.');
            return true;
        }

        $this->model = new $this->model_name;

        $this->action = $data['action'];
        $this->name = ucwords(str_replace('_', ' ', $this->model->table_name()));

        $this->columns = $this->get_columns();

        if ($this->title == '') {
            $this->title = $this->action.' '.$this->model->table_name();
        }
    }

    /**
     * Adds a new non-database mapped column for injecting extra
     * fields into the view.
     *
     * @param string The column key identifier.
     * @param string Pretty display name for the new custom column.
     */
    public function add_column($column_key, $name) {
        if (@in_array($column_key, @$this->disallow[$this->action])) {
            return false;
        }
        $this->fields_names[$column_key] = $name;
        $this->columns[] = array(
            'name'       => $name,
            'column_key' => $column_key,
            'is'         => array($column_key => true),
            'not_db'     => false
        );
    }

    /**
     * Sorts the fields as they are to be displayed.
     *
     * @param array An associative array of the forced order.
     */
    public function set_field_order($order) {
        $this->field_order = $order;
    }

    /**
     * Temporary place holder for the controller, this is expected
     * to be overwritten at the users level.
     */
    public function controller() {}

    /**
     * Defines the model that the scaffold will depend on.
     *
     * @param string The exact model name with namespace reference.
     */
    public function define_model($model) {
        $this->model_name = $model;
    }

    /**
     * A setter to identify which fields not to render in the views.
     *
     * @param string The action to assign the disallow to. Array accepted for
     *               multiple action specifications.
     * @param string Field to disallow.
     */
    public function disallow($action, $field) {
        if (is_array($action)) {
            foreach ($action as $action_key) {
                $this->disallow[$action_key][] = $field;
            }
        } else {
            $this->disallow[$action][] = $field;
        }
        foreach ($this->columns as $key => $column) {
            if ($column['column_key'] == $field) {
                unset($this->columns[$key]);
                return true;
            }
        }
    }

    /**
     * Overrides the pretty field name for a key, in case
     * it's different for a specific view.
     *
     * @param string The column key identifier.
     * @param string Pretty display name for the new custom column.
     */
    public function set_field($key, $name) {
        $this->field_names[$key] = $name;
    }

    /**
     * Adds modifiers to scaffold queries. Used specifically
     * in the list action search feature. Note that this depends
     * on PHPActiveRecords query pattern.
     *
     * @param string The type of query ie. select, conditional etc.
     * @param string Actual query statement to be inserted into the main query.
     */
    public function add_query_modifier($query_type, $query_statement) {
        $this->query_modifiers[$query_type] = $query_statement;
    }

    /**
     * Gets the value from the column array for a specific field.
     *
     * @param string The column name to be retrieved.
     */
    public function get_column_value($field_name) {
        foreach ($this->columns as $key => $column) {
            if ($column['column_key'] == $field_name) {
                return $column['value'];
            }
        }
        return '';
    }

    /**
     * Retrieves a list of columns from the model. TODO: Modify
     * to be agnostic of the model structure, too heavily dependent on
     * PHPActiveRecord.
     *
     * @return array Returns an associative array of the columns with
     *               name, column_key, is (used for Mustache referencing).
     */
    public function get_columns() {

        $columns = array();
        $parcel = explode('\\', $this->model_name);

        foreach ($this->model
                      ->connection()
                      ->columns($this->model->table_name()) as $key => $column) {

            if (isset($this->disallow[$this->action]) && in_array($column->name, $this->disallow[$this->action])) {
                continue;
            }
            if (isset($this->model->field_names[$column->name])) {
                $columns[] = array(
                    'name'       => $this->model->field_names[$column->name],
                    'column_key' => $column->name,
                    'is'         => array($column->name => true)
                );
            } else {
                $columns[] = array(
                    'name'       => $column->name,
                    'column_key' => $column->name,
                    'is'         => array($column->name => true)
                );
            }
        }
        return $columns;
    }

    /**
     * Combines a list of attributes, this is used in the list action search feature.
     *
     * @return string A combined list of fields.
     */
    public function combine_attributes() {
        $attributes = array();
        foreach ($this->columns as $key => $column) {
            if (! isset($column['not_db'])) {
                $attributes[] = $this->model->table_name().'.'.$column['column_key'];
            }
        }
        return join(', ', $attributes);
    }

    /**
     * Detects if a field hook exists for rendering the view. TODO: optimize this function.
     *
     * @param string The name of the field to hook.
     * @param string Accepts both strings and arrays (for the HTML options).
     * @param string Passes the ID of the row to the hook.
     *
     * @return string Returns the translated value of the field.
     */
    public function call_field_hooks($field_name, $value, $id) {
        // Handle hooking for individual actions
        if (method_exists($this, 'hook_'.$this->action.'_'.$field_name)) {
            return $this->{'hook_'.$this->action.'_'.$field_name}($value, $id);
        }

        // Handle hooking for all types
        if (method_exists($this, 'hook_all_'.$field_name)) {
            return $this->{'hook_all_'.$field_name}($value, $id);
        }

        // Handle hooking for edit or create
        if ($this->action == 'edit' || $this->action == 'create') {
            if (method_exists($this, 'hook_edit_and_create_'.$field_name)) {
                return $this->{'hook_edit_and_create_'.$field_name}($value, $id);
            }
            if (method_exists($this, 'hook_create_and_edit'.$field_name)) {
                return $this->{'hook_create_and_edit'.$field_name}($value, $id);
            }
        }

        // Handle hooking for edit or create
        if ($this->action == 'list' || $this->action == 'view') {
            if (method_exists($this, 'hook_list_and_view_'.$field_name)) {
                return $this->{'hook_list_and_view_'.$field_name}($value, $id);
            }
            if (method_exists($this, 'hook_view_and_list_'.$field_name)) {
                return $this->{'hook_view_and_list_'.$field_name}($value, $id);
            }
        }
        return $value;
    }

    /**
     * A helper used with field hooks to render out more usable
     * interface options.
     *
     * @param string Field type to interpret, accepted values: 
     *               select, textarea, radio, html, upload. Defaults to input field.
     *
     * @return string Returns the output of the HTML generated.
     */
    public function generate_field_html(&$field) {
        if (is_array($field['value']) && ! is_object($field['value'])) {
            $type = $field['value']['type'];
        } else {
            $type = 'text';
        }
        switch ($type) {
            case 'select':
                $output = '<select name="fields['.str_replace('"', '', $field['column_key']).']">';
                foreach ($field['value']['options'] as $key => $value) {
                    if ($field['value']['value'] == $key) {
                        $output .= '<option value="'.$key.'" selected="selected">'.$value.'</option>'."\n";
                    } else {
                        $output .= '<option value="'.$key.'">'.$value.'</option>'."\n";
                    }
                }
                $output .= '</select>';
                break;
            case 'textarea':
                $output = '<textarea '.@$field['value']['attributes'].
                          ' name="fields['.$field['column_key'].']">'.
                          $field['value']['value'].'</textarea>';
                break;
            case 'radio':
                $output = '';
                foreach ($field['value']['options'] as $key => $value) {
                    $output .= '<input name="fields['.$field['column_key'].']" '.
                               'type="radio" name="'.$field['column_key'].'" value="'.$field['value'].'">';
                }
                break;
            case 'html':
                $output = $field['value']['value'];
                break;
            case 'upload':
                $output = '<input name="fields['.$field['column_key'].']" type="file">';
                break;
            default:
                $output = '<input name="fields['.$field['column_key'].']" '.
                          'type="text" value="'.htmlentities($field['value']).'">';
                break;
        }
        return $output;
    }

    /**
     * Maps the current scaffold action to the proper view. If name 
     * not specified, defaults to finding the root parcel views.
     *
     * @param string Name of the view according to Pigeon namespace
     *               ie. Sample\View\Sample_Scaffold_Edit
     */
    public function set_view($name = '') {
        if ($name != '') {
            $this->view = $name;
        } else {
            $parcel = explode('\\', get_called_class());
            $view = strtolower(PARCEL_ROOT.$parcel[0].DS.'views'.DS.strtolower($parcel[2]).'_scaffold_'.$this->action.'.php');
            
            if (is_file($view)) {
                $this->view = $parcel[0].'\View\\'.$parcel[2] . '_scaffold_' . $this->action;
            } else {
                $this->view = BASE_PARCEL.'\View\Scaffold_'.$this->action;
            }
        }
    }

    /**
     * Sorts the columns according to the specified field order.
     *
     * @param array A list of current columns.
     * @param array The order array to map the column list against.
     */
    public function sort_columns(&$columns, &$field_order) {
        // Sort the fields before they get sent out
        foreach ($columns as $key => $field) {
            $sort = array_search($field['column_key'], $field_order);
            if ($sort == '') {
                $sort = '9999';
            }
            $columns[$key]['sort'] = $sort;
        }
        usort($columns, function($a, $b) {
            if ($a['sort'] == $b['sort']) {
                return 0;
            }
            return ($a['sort'] < $b['sort']) ? -1 : 1;
        });
    }

    /**
     * Saves any incoming data, checks the value against the save hook.
     *
     * @param array The post value sent to the controller.
     * @param string The ID of the current row being modified.
     *
     * @return boolean Returns true if succeeded. TODO: Make return false
     *                 if there's a save error.
     */
    public function save_data($post, $id = '') {
        if ($id != '') {
            $row = $this->model->find($id);
        } else {
            $row = $this->model;
        }
        $this->row = $row;
        $method = 'hook_save_'.$this->action;
        if (method_exists($this, $method)) {
            $this->{$method}($id);
        }
        foreach ($this->columns as $i => $column) {
            $method = 'hook_save_'.$this->action.'_'.$column['column_key'];
            if (method_exists($this, $method)) {
                $post[$column['column_key']] = $this->{$method}($post[$column['column_key']], $id);
            }
            if (isset($column['not_db'])) {
                continue;
            } 
            if (! isset($this->disallow[$this->action][$column['column_key']]) &&
                isset($post[$column['column_key']])) {
                $row->{$column['column_key']} = stripslashes($post[$column['column_key']]);
            }
        }
        $row->save();
        return true;
    }

    /**
     * Performs the actions of the scaffold by calling static subclasses
     * that will perform the desired action.
     */
    protected function generate() {

        if (count($this->field_order) != 0) {
            $this->sort_columns($this->columns, $this->field_order);
        }
        
        if (! $this->{'allow_'.$this->action}) {
            throw new \Pigeon\Core\Exception('Sorry but that action is not allowed.');
        }
        
        switch ($this->action) {

            // Perform the create action
            case 'create':
                \Pigeon\Core\Scaffold_Create_Controller::generate($this);
                break;

            // Perform the delete action
            case 'list':
                \Pigeon\Core\Scaffold_List_Controller::generate($this);
                break;

            // Perform the row edit option
            case 'view':
                \Pigeon\Core\Scaffold_View_Controller::generate($this);
                break;

            // Perform the row edit option
            case 'edit':
                \Pigeon\Core\Scaffold_Edit_Controller::generate($this);
                break;
                
            // Perform the row delete action
            case 'delete':
                $row = $this->model->find($_GET[current($this->model->get_primary_key())]);
                $row->delete();
                $this->delete_success = true;
                break;
                
            default:
                die('No action found');
                break;
        }
        $this->set_view();
    }
    
}
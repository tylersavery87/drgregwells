<?php
namespace Core\Controllers;

class Static_404_Controller extends \Core\Controllers\Base_Controller {
	
	function controller() {
	    header('HTTP/1.0 404 Not Found');
		$this->title = '404 Page Not Found';
	    $this->set_view('Core\View\Static_404');
	}
	
}
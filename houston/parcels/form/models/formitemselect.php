<?php

namespace Form\Models;

class FormItemSelect extends FormItem {
    
    public function __construct($attributes){
        $this->type = 'select';
        $this->template = 'select';
        parent::__construct($attributes);
    }
    
    
    
}
<?php

namespace Form\Models;

class FormItemHidden extends FormItem {
    
    public function __construct($attributes){
        $this->type = 'hidden';
        $this->template = 'hidden';
        parent::__construct($attributes);
    }
    
    
    
}
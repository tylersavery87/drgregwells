<?php

namespace Form\Models;

class FormItemRadio extends FormItem {
    
    public function __construct($attributes){
        $this->type = 'radio';
        $this->template = 'radio';
        parent::__construct($attributes);
    }
    
    
    
}
<?php

namespace Form\Models;

class FormItemImagePreview extends FormItem {
    
    public function __construct($attributes){
        $this->type = 'image';
        $this->template = 'image';
        parent::__construct($attributes);
    }
    
    
    
}
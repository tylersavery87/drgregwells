<?php

namespace Form\Models;

class FormItemAudioPreview extends FormItem {
    
    public function __construct($attributes){
        $this->type = 'audio';
        $this->template = 'audio';
        parent::__construct($attributes);
    }
       
}
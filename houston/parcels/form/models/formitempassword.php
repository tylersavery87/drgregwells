<?php

namespace Form\Models;

class FormItemPassword extends FormItem {
    
    public function __construct($attributes){
        $this->type = 'password';
        $this->template = 'password';
        parent::__construct($attributes);
    }
    
    
    
}
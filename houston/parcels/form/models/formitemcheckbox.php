<?php

namespace Form\Models;

class FormItemCheckbox extends FormItem {
    
    public function __construct($attributes){
        $this->type = 'checkbox';
        $this->template = 'checkbox';
        parent::__construct($attributes);
    }
    
    
    
}
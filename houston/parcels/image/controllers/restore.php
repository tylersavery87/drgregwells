<?php
namespace Image\Controllers;

class Restore extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$image = \Image\Models\Image::find($data["image_id"]);

		if($image){
			$image->deleted = 0;
			$image->save();
			$this->add_flash(array("message" => "Image has been restored!"));

			redirect_to($image->link_all());
		}
	}
}

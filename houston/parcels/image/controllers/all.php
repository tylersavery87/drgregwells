<?php
namespace Image\Controllers;

class All extends \Manage\Controllers\Manage{

	private $images;

	public $images_data = array();
	public $link_new;
	public $page_title;
	public $has_data = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Images";
		$this->link_new = \Image\Models\Image::link_new();

		$this->images = \Image\Models\Image::all(array("conditions" => "deleted = 0"));
		$archived = \Image\Models\Image::count(array("conditions" => "deleted = 1"));
		if($archived > 0){
			$this->archive_button = array("url" => \Image\Models\Image::link_archive(), "title" => "Archive");
		}

		foreach($this->images as $image){
			$this->has_data = true;
			$this->images_data[] = array(
				"id" => $image->id,
				"filename" => $image->filename,
				"title" => $image->title,
				"link_edit" => $image->link_edit(),
				"link_delete" => $image->link_delete(),
			);
		}

	}

	public function controller(){
		$this->set_view("Image\Views\All");
	}

}


<?php
namespace Image\Controllers;

class Archive extends \Manage\Controllers\Manage{

	private $images;

	public $images_data = array();
	public $link_new;
	public $page_title;
	public $has_data = false;
	public $archive_button = false;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$this->page_title = "Deleted Images";

		$this->images = \Image\Models\Image::all(array("conditions" => "deleted = 1"));
		$this->archive_button = array("url" => \Image\Models\Image::link_all(), "title" => "Back");
		foreach($this->images as $image){
			$this->has_data = true;
			$this->images_data[] = array(
				"id" => $image->id,
				"filename" => $image->filename,
				"title" => $image->title,
				"link_restore" => $image->link_restore(),
			);
		}

	}

	public function controller(){
		$this->set_view("Image\Views\All");
	}

}


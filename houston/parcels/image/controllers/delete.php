<?php
namespace Image\Controllers;

class Delete extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$image = \Image\Models\Image::find($data["image_id"]);

		if($image){
			$image->deleted = 1;
			$image->save();
			$this->add_undo(array("message" => "Image has been deleted!", "action" => $image->link_restore()));

			redirect_to($image->link_all());
		}
	}
}

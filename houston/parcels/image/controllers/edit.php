<?php
namespace Image\Controllers;

class Edit extends \Manage\Controllers\Manage{

	private $image;

	public $page_title;
	public $link_back;
	public $form;
	public $existing;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		if(isset($_POST["submit"])){
			$this->submit_form();
		}

		if(isset($data["image_id"])){
			$this->image = \Image\Models\Image::find($data["image_id"]);
			$this->page_title = "Editing Image";
		} else {
			$this->image = new \Image\Models\Image;
			$this->page_title = "New Image";
		}

		$this->link_back = $this->image->link_all();

		$form = new \Form\Models\Form("edit", "POST", "", "well");

		$item = new \Form\Models\FormItemHidden(array("id" => "id", "title" => "#", "value" => $this->image->id));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "filename", "title" => "Filename", "value" => $this->image->filename));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "title", "title" => "Title", "value" => $this->image->title));
		$form->add_item($item);

		$item = new \Form\Models\FormItemTextarea(array("id" => "caption", "title" => "Caption", "value" => $this->image->caption));
		$form->add_item($item);

		$item = new \Form\Models\FormItemSubmit(array("value" => "Save", "id" => "submit", "class" => "btn btn-primary", "cancel_url" => $this->image->link_all()));
		$form->add_item($item);

		$this->form = $form->render();

	}

	public function controller(){
		$this->set_view("Image\Views\Edit");
	}

	private function submit_form(){
		if($_POST["id"] > 0){
			$image = \Image\Models\Image::find($_POST["id"]);
			$flash_msg = "Image has been updated successfully.";
		} else {
			$image = new \Image\Models\Image;
			$flash_msg = "Image has been created successfully.";
		}

		$image->filename = $_POST["filename"];
		$image->title = $_POST["title"];
		$image->caption = $_POST["caption"];

		$image->save();
		$this->add_flash(array("message" => $flash_msg, "type" => "success", "Heading" => "Great!"));
		redirect_to($image->link_all());
	}

}

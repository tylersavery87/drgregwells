<?php

namespace Api\Controllers;

class Get extends \Core\Controllers\Api_Controller {

	public function __construct($uri, $data){
        parent::__construct($uri, $data);

        $hash_id = (int)$data['hash_id'];

        $hashtag = \Hashtag\Models\Hashtag::find($hash_id);

        $photos = $hashtag->get_photos((int)$_POST['amount']);

        foreach($photos as $photo){
            
            $upvotes = \Vote\Models\Vote::count(array('conditions' => 'photo_id = ' . $photo->id . ' AND value = 1'));
            $downvotes = \Vote\Models\Vote::count(array('conditions' => 'photo_id = ' . $photo->id . ' AND value = -1'));

            $this->json_response['photos'][] = array(
                'id' => $photo->id,
                'url' => $photo->url,
                'username' => $photo->username,
                'user_photo' => $photo->user_photo,
                'upvotes' => $upvotes,
                'downvotes' => $downvotes

            );

        }

    	$this->response_code = 200;    	

    	$this->render_view();

	}

}
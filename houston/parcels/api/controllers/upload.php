<?php

namespace Api\Controllers;

class Upload extends \Core\Controllers\Api_Controller {

	protected $upload_type;
	protected $file_name;
	protected $file_type;
	protected $file_extension;
	protected $file_tmpname;
	protected $file_error;
	protected $file_size;
	protected $target_path;
	protected $upload_response = array();
	protected $absolute_path_public;
	protected $absolute_path_private;
	protected $cmds = array();

	protected $upload_errors = array();

	protected $file_meta = array();

	public function __construct($uri, $data) {
        parent::__construct($uri, $data);

        $this->upload_type = $data['type'];

        $this->set_params($_FILES['files']);
        
        if(!$this->validate_params()){
        	$this->error_out();
        }

        if($this->upload_file()) {
        	$this->upload_response[] = array(
        		'type' => $this->upload_type,
        		'name' => $this->file_name,
        		'size' => $this->file_size,
        		'url' => $this->absolute_path_public,
        		'thumbnail_url' => $this->absolute_path_public,
        		'meta' => $this->file_meta,
        		'cmds' => $this->cmds
        	);

        	$this->execute_convert_cue();
        	$this->send_upload_reponse();
        	die();

        }

    }

    protected function upload_file() {
    	$this->target_path = UPLOAD_DIRECTORY . $this->upload_type . 's' . DS . basename($this->file_name);

    	if(!move_uploaded_file($this->file_tmpname, $this->target_path)) {
    		return false; // add error info
    	}

    	$this->absolute_path_public = BASE_URL . 'uploads/' . $this->upload_type . 's/' . $this->file_name; 
        $this->absolute_path_private = $this->target_path; 

    	switch($this->upload_type){
    		case "track":
    			$this->parse_audio(true);
    			break;
    		case "image":
    			
    			break;
    	}

    	return true;

    }


    protected function parse_audio($convert = false) {

    	require_once(LIBRARY_ROOT . 'getid3' . DS . 'getid3.php');

    	$getID3 = new \getID3();
    	$file_info = $getID3->analyze($this->absolute_path_private);

    	$this->file_meta = array(
    		'playtime' => $file_info['playtime_string'],
    		'seconds' => $file_info['playtime_seconds'],
    		'bitrate' => $file_info['audio']['bitrate'],
    		'sample_rate' => $file_info['audio']['sample_rate'],
    		'title' => $file_info['tags']['id3v2']['title'][0],
    		'artist' => $file_info['tags']['id3v2']['artist'][0],
    		'album' => $file_info['tags']['id3v2']['album'][0]
    	);

  		//oga file
        $this->add_to_convert_cue($this->absolute_path_private, $file_extension, 'ogg');


    }

    protected function set_params($file) {

    	$this->file_name = time() . '_' . safe_filename($file['name'][0]);
    	$this->file_type = $file['type'][0];
    	$this->file_tmpname = $file['tmp_name'][0];
    	$this->file_error = $file['error'][0];
    	$this->file_size = $file['size'][0];
    	$this->file_extension = get_extension($this->file_name);

    	switch($this->upload_type){
    		case "track":
    			$this->allowed_extensions = array('mp3', 'wav', 'ogg', 'oga', 'aif');
    			break;
    		case "image":
    			$this->allowed_extensions = array('jpg', 'jpeg', 'gif', 'png');
    			break;
    	}

    }

    protected function validate_params() {
    	if($this->file_error != 0){
    		return false;
    	}

    	if (!in_array(end(explode(".", strtolower($this->file_name))), $this->allowed_extensions)) {
    		$this->upload_errors[] = 'Invalid file. Accepted file types: ' . join(', ', $this->allowed_extensions);
    		return false;
    	}

    	return true;
    }

    protected function send_upload_reponse() {
    
    	header('Content-type: application/json');

        echo json_encode($this->upload_response);
    
    }

    protected function add_to_convert_cue($file, $exec_in, $ext_out, $verbose = true) {
        
        $newfile = str_replace($exec_in, $ext_out, $file);

        $trail = ($verbose) ? " &> /dev/null &" : " 2>&1";

        $this->cmds[] = FFMPEG . " -i ". $file ." -ab 48000 -acodec libvorbis " . $newfile . $trail;
    }

    protected function execute_convert_cue() {

    	foreach($this->cmds as $cmd) {
    		shell_exec($cmd);
    	}

    }


    protected function error_out(){
    	header('Content-type: application/json');
        echo json_encode(array('errors' => $this->upload_errors));

        die();
    }

}
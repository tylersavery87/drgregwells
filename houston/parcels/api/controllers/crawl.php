<?php

namespace Api\Controllers;

class Crawl extends \Core\Controllers\Api_Controller {

    protected $i = 0;
    protected $page = 1;
    protected $max_pages = 50;
    protected $hashtag_id;

	public function __construct($uri, $data){
        parent::__construct($uri, $data);

        $this->hashtag_id = (int)$data['hash_id'];
        $hashtag = \Hashtag\Models\Hashtag::find($this->hashtag_id);

        $api_url = "https://api.instagram.com/v1/tags/". $hashtag->hash  ."/media/recent?access_token=38791575.f59def8.640a56fa29144de3a819cbd3e5980163";
        $results = json_decode(file_get_contents($api_url));

        $this->add_images($results);

        echo "i: " + $this->i . "<br>";
        echo "pages: " + $this->page . "<br>";
       
    	$this->response_code = 200;    	

    	$this->render_view();

	}

    private function add_images($results) {

        $next_url = $results->pagination->next_url;

        foreach($results->data as $result){

            $count = \Photo\Models\Photo::count(array('conditions' => "uid = '" . $result->id . "'"));
                if($count == 0){

                echo '<img src="' . $result->images->standard_resolution->url . '" width=300 /><br />';
                $photo = new \Photo\Models\Photo;
                $photo->url = $result->images->standard_resolution->url;
                $photo->hashtag_id = $this->hashtag_id;
                $photo->entry_datetime = time();

                $photo->uid = $result->id;
                $photo->username = $result->user->username;
                $photo->user_photo = $result->user->profile_picture;

                $photo->save();
                $this->i++;
            }
        }

        if($next_url && $this->page < $this->max_pages){
            $this->page++;
            echo "--NEW PAGE--";
            $results = json_decode(file_get_contents($next_url));
            $this->add_images($results);
        }

    }

}
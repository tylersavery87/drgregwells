<?php

namespace Api\Controllers;

class Choose extends \Core\Controllers\Api_Controller {

	public function __construct($uri, $data){
        parent::__construct($uri, $data);

        $winner_id = (int)$_POST['winner_id'];
        $loser_id = (int)$_POST['loser_id'];

        $win_vote = new \Vote\Models\Vote;
        $win_vote->photo_id = $winner_id;
        $win_vote->value = 1;
        $win_vote->entry_datetime = time();
        $win_vote->ip_address = $_SERVER['REMOTE_ADDR'];
        $win_vote->save();

        $lose_vote = new \Vote\Models\Vote;
        $lose_vote->photo_id = $loser_id;
        $lose_vote->value = -1;
        $lose_vote->entry_datetime = time();
        $lose_vote->ip_address = $_SERVER['REMOTE_ADDR'];
        $lose_vote->save();


    	$this->response_code = 200;    	
    	$this->json_response['response'] = json_decode(file_get_contents(BASE_URL . 'api/get/1'));
    	$this->json_response['winner_id'] = $winner_id;
    	$this->json_response['loser_id'] = $loser_id;


    	$this->render_view();

	}

}
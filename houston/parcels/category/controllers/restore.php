<?php
namespace Category\Controllers;

class Restore extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$category = \Category\Models\Category::find($data["category_id"]);

		if($category){
			$category->deleted = 0;
			$category->save();
			$this->add_flash(array("message" => "Category has been restored!"));

			redirect_to($category->link_all());
		}
	}
}

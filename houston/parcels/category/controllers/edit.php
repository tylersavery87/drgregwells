<?php
namespace Category\Controllers;

class Edit extends \Manage\Controllers\Manage{

	private $category;

	public $page_title;
	public $link_back;
	public $form;
	public $existing;

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		if(isset($_POST["submit"])){
			$this->submit_form();
		}

		if(isset($data["category_id"])){
			$this->category = \Category\Models\Category::find($data["category_id"]);
			$this->page_title = "Editing Category";
		} else {
			$this->category = new \Category\Models\Category;
			$this->page_title = "New Category";
		}

		$this->link_back = $this->category->link_all();

		$form = new \Form\Models\Form("edit", "POST", "", "well");

		$item = new \Form\Models\FormItemHidden(array("id" => "id", "title" => "#", "value" => $this->category->id));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "title", "title" => "Title", "value" => $this->category->title));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "slug", "title" => "Slug", "value" => $this->category->slug));
		$form->add_item($item);

		$item = new \Form\Models\FormItemText(array("id" => "parent_id", "title" => "Parent Category", "value" => $this->category->parent_id));
		$form->add_item($item);

		$item = new \Form\Models\FormItemSubmit(array("value" => "Save", "id" => "submit", "class" => "btn btn-primary", "cancel_url" => $this->category->link_all()));
		$form->add_item($item);

		$this->form = $form->render();

	}

	public function controller(){
		$this->set_view("Category\Views\Edit");
	}

	private function submit_form(){
		if($_POST["id"] > 0){
			$category = \Category\Models\Category::find($_POST["id"]);
			$flash_msg = "Category has been updated successfully.";
		} else {
			$category = new \Category\Models\Category;
			$flash_msg = "Category has been created successfully.";
		}

		$category->title = $_POST["title"];
		$category->slug = $_POST["slug"];
		$category->parent_id = $_POST["parent_id"];

		$category->save();
		$this->add_flash(array("message" => $flash_msg, "type" => "success", "Heading" => "Great!"));
		redirect_to($category->link_all());
	}

}

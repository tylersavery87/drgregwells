<?php
namespace Category\Controllers;

class Delete extends \Manage\Controllers\Manage{

	public function __construct($uri, $data){
		parent::__construct($uri, $data);

		$category = \Category\Models\Category::find($data["category_id"]);

		if($category){
			$category->deleted = 1;
			$category->save();
			$this->add_undo(array("message" => "Category has been deleted!", "action" => $category->link_restore()));

			redirect_to($category->link_all());
		}
	}
}

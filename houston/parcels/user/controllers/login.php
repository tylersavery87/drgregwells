<?php
namespace User\Controllers;

class Login extends \User\Controllers\User {
    
    public $login_form_partial;
    
    public function __construct($uri, $data) {
        parent::__construct($uri, $data);
        
        $this->add_asset('css', 'fbootstrap.css', true);
        $this->add_asset('css', 'manage.css', true);
        
        $username = '';

        if(isset($_POST['submit']) || isset($_POST['api'])){
            $this->submit_form();
            $username = $_POST['username'];
        }
        
        $form = new \Form\Models\Form('login');
        
        $item = new \Form\Models\FormItemText(array('id' => 'username', 'value' => $username, 'required' => false, 'title' => 'Username'));
        $form->add_item($item);
        
        $item = new \Form\Models\FormItemPassword(array('id' => 'password', 'value' => '', 'required' => false, 'title' => 'Password'));
        $form->add_item($item);
        
        $item = new \Form\Models\FormItemSubmit(array('value' => 'Submit', 'id' => 'submit', 'class'=> 'btn btn-primary'));
        $form->add_item($item);
        
        $this->login_form_partial = $form->render();
        
        $this->set_view('User\Views\Login');
        
    }
    
    
    protected function submit_form(){
        
        $user = new \User\Models\User();
        $user->username = $_POST['username'];
        $user->password = $_POST['password'];
        
        $api = false;
        if(isset($_POST['api']) && (int)$_POST['api'] == 1){
            $api = true;
        }
        
        $login = $user->login();
        
        if($login){
            
            $this->add_flash(array('message' => 'Welcome Back ' . $login->firstname . "!", 'type' => 'success'));
            redirect_to("/manage/hashtags");
      
        } else {
            if($api){
                $this->json_responses = array('code' => "500");
                $this->return_reponse();
            } else {
                $this->set_errors($user->errors); 
            }
            
            
        }
        
    }
    
    
}
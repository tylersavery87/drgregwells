<?php
namespace User\Controllers;

class Profile extends \User\Controllers\User {
    
    public function __construct($uri, $data) {
        parent::__construct($uri, $data);

        $user_id = $data['user_id'];
        $user = \User\Models\User::find($user_id);
        $user_info = $user->api_get_user();
   
        $this->json_responses = $user_info;
        $this->return_reponse();
    }
    
}
<?php
namespace User\Controllers;

class Reset extends \User\Controllers\User {
    
    public function __construct($uri, $data) {
        parent::__construct($uri, $data);
        
        $this->set_view('User\View\Reset');
        
    }
    
}
<?php

namespace Frontend\Controllers;

class Frontend extends \Core\Controllers\Base_Controller {

	public function __construct($uri, $data){
        parent::__construct($uri, $data);

        $this->add_asset('css', 'reset.css', 'manage');
        $this->add_asset('css', 'frontend.css', true);
        $this->add_asset('js', 'jquery.min.js', 'core');

        
    }

}
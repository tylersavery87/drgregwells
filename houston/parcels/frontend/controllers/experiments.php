<?php

namespace Frontend\Controllers;

class Experiments extends \Frontend\Controllers\Frontend {


	public function __construct($uri, $data) {
        parent::__construct($uri, $data);

        $this->add_asset('css', 'experiments.css', true);

    }

    public function controller() {
		$this->set_view('Frontend\Views\Experiments');
	}

}
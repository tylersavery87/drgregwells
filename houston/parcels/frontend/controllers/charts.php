<?php

namespace Frontend\Controllers;

class Charts extends \Frontend\Controllers\Experiments {


	public function __construct($uri, $data) {
        parent::__construct($uri, $data);

    }

    public function controller() {
		$this->set_view('Frontend\Views\Charts');
	}

}
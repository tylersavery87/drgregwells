<?php

namespace Frontend\Controllers;

class Index extends \Frontend\Controllers\Frontend {

    public $hashtags;

	public function __construct($uri, $data){
        parent::__construct($uri, $data);

        $this->add_asset('css', 'main.css', true);

    }

    public function controller(){
		$this->set_view('Frontend\Views\Index');
	}

}
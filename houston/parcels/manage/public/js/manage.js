(function($) {
$.fn.serializeFormJSON = function() {

   var o = {};
   var a = this.serializeArray();
   $.each(a, function() {
       if (o[this.name]) {
           if (!o[this.name].push) {
               o[this.name] = [o[this.name]];
           }
           o[this.name].push(this.value || '');
       } else {
           o[this.name] = this.value || '';
       }
   });
   return o;
};
})(jQuery);


$(document).ready(function(){
   
    $(".btn-danger").click(function(){
        return confirm("Are you sure?"); 
    });
    
    $(".reorder.up").click(function(){
       var parcel = $(this).attr('rel') 
       reorder($(this), 'up', parcel);
       return false;
    });
    
    $(".reorder.down").click(function(){
        
       var parcel = $(this).attr('rel') 
       reorder($(this), 'down', parcel);
       return false;
    });
    

    $(".datepicker").datepicker({ dateFormat: "MM d, yy"});
    
    $(".colorpicker").miniColors();

    $('.tip').tooltip();


    $(".box form input[type=submit]").click(function(){
        var $btn = $(this);
        var $form = $(this).parent().parent();
        var $box = $form.parent();
        var $loading = $box.find('.loading');

        var data = $form.serializeFormJSON();

        var btn_text = $(this).attr('value');
        $btn.val('Loading...');
        $btn.attr('disabled', true);
        $loading.fadeIn(300);

        
        $.ajax({
            type: "POST",
            url: "/api/settings/" + data.type,
            dataType: 'json',
            data: data
        }).success(function(msg) {
            var resp = msg;
            $box.find(".label.not-connected").fadeOut(400);
            $box.find(".label.error").fadeOut(400);
            $box.find(".label.connected").fadeIn(400);

            $loading.fadeOut(300);
            $btn.attr('disabled', false);
            $btn.val(btn_text);

        }).error(function(msg){
            var resp = $.parseJSON(msg.responseText);
            $box.find(".label.not-connected").fadeOut(400);
            $box.find(".label.connected").fadeOut(400);
            //$box.find(".label.error").attr('title', 'poop');
            $box.find(".label.error").tooltip('destroy');
            $box.find(".label.error").tooltip({'title':resp.msg});
            $box.find(".label.error").fadeIn(400);

            $loading.fadeOut(300);
            $btn.attr('disabled', false);
            $btn.val(btn_text);

        });

        

        return false;
    });
    
  
    
});


function reorder($btn, direction, parcel){
    
    var $row = $btn.parent().parent();
    var this_id = parseInt($row.attr('rel'));
    
    //get swap id
    if(direction == 'up'){
        var $swap_row = $row.prev();
        $swap_row.before($row);
        
    } else{
        var $swap_row = $row.next();
        $swap_row.after($row);
    }
    
    fix_reordered_numbers();

    var swap_id = parseInt($swap_row.attr('rel'));
    
    $.ajax({
        type: "POST",
        url: "/manage/"+parcel+"/reorder/" + this_id + "/" + swap_id,
        data: { }
    }).done(function( msg ) {
        console.log(msg);
        hide_first_and_last_reorder();
    });
    
    
}

<?php

namespace Manage\Controllers;

class Manage extends \Core\Controllers\Base_Controller {

    protected $_user;
    
    public function __construct($uri, $data){
        parent::__construct($uri, $data);
        
        $this->title = 'JackieBoys :: Control Panel';
        
        $this->add_asset('css', 'reset.css', 'manage');
        $this->add_asset('css', 'united.bootstrap.min.css', 'manage');
        $this->add_asset('css', 'custom-theme/jquery-ui.css', true);
        $this->add_asset('css', 'minicolors.css', 'manage');
        $this->add_asset('css', 'manage.css', 'manage');
        
        $this->add_asset('js', 'jquery.min.js', 'core');
        $this->add_asset('js', 'bootstrap.min.js', 'manage');
        $this->add_asset('js', 'jquery.ui.core.js', 'manage');
        $this->add_asset('js', 'jquery.ui.widget.js', 'manage');
        $this->add_asset('js', 'jquery.ui.datepicker.js', 'manage');
        $this->add_asset('js', 'minicolors.js', 'manage');
		$this->add_asset('js', 'manage.js', 'manage');
        
        $this->unprotected_controllers = array('User\\Controllers\\Login', 'User\\Controllers\\Logout');
        
        $this->authenticate();
        
    }
    
    public function controller(){
		return;
	}
    
    
    public function authenticate() {
		
        $user = new \User\Models\User();
    
        if(!isset($_SESSION[$user->login_session_name]) || $_SESSION[$user->login_session_name] < 1){
	    
            if(!in_array(get_called_class(), $this->unprotected_controllers)){
            $this->add_flash(array('message' => 'You are not permitted to view this page'));
                redirect_to('/users/login');
            }
        }
		
        if(isset($_SESSION[$user->login_session_name])){
            $this->_user = $user->get_logged_in_user();
        }
		
    }


    protected function get_background_types($current){
        
        $bg_types = \Landing\Models\Backgroundtype::all();
        
        $a = array();
        foreach($bg_types as $type){
            if($type->id == $current) { $selected = true; } else { $selected = false; }
            $a[] = array('value' => $type->id, 'title' => $type->title, 'slug' => $type->slug, 'selected' => $selected);
        }
        
        return $a; 

    }
    
}
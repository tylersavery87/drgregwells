<?php

namespace Manage\Controllers;

class Dashboard extends \Manage\Controllers\Manage {

	public $partials = array();
	public $forms = array();
    
    public function __construct($uri, $data){
        parent::__construct($uri, $data);
        $this->test = "hey there tyler";

        $this->init_partials('email');

    }

    public function controller(){
    	$this->set_view("Manage\Views\Dashboard");
    }

    private function init_partials($partial){

        switch(strtolower($partial)){
            case "email":
                $form = new \Form\Models\Form("edit", "POST", "", "well");

                $item = new \Form\Models\FormItemHidden(array("id" => "type", "value" => "email"));
                $form->add_item($item);

                $item = new \Form\Models\FormItemText(array("id" => "email", "title" => "My Email Address:", "value" => ""));
                $form->add_item($item);

                $item = new \Form\Models\FormItemSubmit(array("value" => "Save", "id" => "submit", "class" => "btn btn-primary", "cancel_url" => false));
                $form->add_item($item);

                $this->forms['email'] = $form->render();
                $this->partials['settings']['email'] = $this->get_partial("Manage\Views\Settings_Email");
                
            break;
        }

    }

}